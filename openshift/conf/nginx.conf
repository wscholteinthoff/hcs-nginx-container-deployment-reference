worker_processes auto;
error_log stderr info;
pid /run/nginx.pid;

events {
  worker_connections  1024;
}

http {

  #Logging format
  log_format json_combined escape=json
   '{'
    '"time_local":"$time_local",'
    '"msec":"$msec",'
    '"remote_addr":"$remote_addr",'
    '"remote_user":"$remote_user",'
    '"request":"$request",'
    '"request_length":"$request_length",'
    '"request_method":"$request_method",'
    '"request_uri":"$request_uri",'
    '"server_port":"$server_port",'
    '"server_protocol":"$server_protocol",'
    '"ssl_protocol":"$ssl_protocol",'
    '"status": "$status",'
    '"body_bytes_sent":"$body_bytes_sent",'
    '"bytes_sent":"$bytes_sent",'
    '"request_time":"$request_time",'
    '"http_host":"$http_host",'
    '"http_referrer":"$http_referer",'
    '"http_user_agent":"$http_user_agent",'
    '"http_x_forwarded_for":"$http_x_forwarded_for",'
    '"upstream_response_time":"$upstream_response_time",'
    '"upstream_addr":"$upstream_addr",'
    '"upstream_connect_time":"$upstream_connect_time"'
   '}';

  #Additional http settings
  access_log /dev/stdout json_combined;
  sendfile    on;
  tcp_nopush    on;
  tcp_nodelay   on;
  keepalive_timeout  65;
  types_hash_max_size 2048;
  server_tokens off;

  #Block traffic from outside platform for /stub_status
  map $remote_addr $block_outside_pod {
    default               1;
    127.0.0.1             0;
    ~^::1$                0;
  }

  #Block traffic from outside platform for /metrics
  map $http_x_forwarded_for $block_outside_platf orm {
    default               1;
    ~^123\.45\.\d*.\d*$   0;
    ~^123\.46\.\d*.\d*$   0;
  }

  server {
    listen     8080 default_server;
    listen     [::]:8080 default_server;
    server_name  _;
    root     /opt/app-root/src;

    # Metrics
    error_page 418 = @block_traffic;

    location /stub_status {
      #sidecar only
      if ($block_outside_pod = 1) {
        return 418;
      }
      stub_status;
    }

    location /metrics {
      #platform only
      if ($block_outside_platform = 1) {
        return 418;
      }
    }

    location @block_traffic {
      rewrite ^ /index.html$1;
      proxy_pass http://localhost:8080;
    }

    # Main Application
    location /ping {
      add_header Content-Type text/plain;
      add_header Cache-Control "no-store, no-cache, must-revalidate, post-check=0, pre-check=0";
      add_header X-Content-Type-Options nosniff;
      add_header X-Frame-Options "SAMEORIGIN";
      add_header X-XSS-Protection "1; mode=block";
      add_header Referrer-Policy "same-origin" always; 
      return 200 'pong'; 
    }

    # Landing Page
    location / {
      add_header Content-Type text/plain;
      add_header Cache-Control "no-store, no-cache, must-revalidate, post-check=0, pre-check=0";
      add_header X-Content-Type-Options nosniff;
      add_header X-Frame-Options "SAMEORIGIN";
      add_header X-XSS-Protection "1; mode=block";
      return 200 'These are not the bytes you are looking for, try /ping instead...';
    }
  }
}

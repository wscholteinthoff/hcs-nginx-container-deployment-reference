# README

# Introduction 
This git has been created as reference for a blog I wrote. See the following URL for the blog post: [placeholder](www.hcs-company.com)

## Table of Conetents
<!-- vim-markdown-toc GitLab -->

* [Architecture](#architecture)
* [Prerequisites](#prerequisites)
* [The Base Image](#the-base-image)
* [Building and Deploying the NGINX and Prometheus Node Exporter application](#building-and-deploying-the-nginx-and-prometheus-node-exporter-application)
  * [The Build Template](#the-build-template)
  * [The Deploy Template](#the-deploy-template)
* [Building and Deploying the Alert Template](#building-and-deploying-the-alert-template)
* [Grafana Dashboard](#grafana-dashboard)
  * [Steps on how to import the Grafana Json](#steps-on-how-to-import-the-grafana-json)


<!-- vim-markdown-toc -->

# Architecture
A picture says more than a 1000 words. For this reason, see the simplified architectural representation of this deployment below.

<div align="center">
![architecture](/pictures/architecture.png "Architectural Representation")
</div>

# Prerequisites
- Please make sure you have access to the S2I Image [rhel8/nginx-120](https://catalog.redhat.com/software/containers/rhel8/nginx-120/6156abe6075b022acc111afe), you can use this as your starting point image so you can create your own baseImage.
- I added some modifications to the Dockerfile to make it usable for my use case. If you want to reach https websites from the POD within your own organization (for example a package management solution), please make sure you also import your own Certificate Root bundles. Review the 'Dockerfile' file (line 31) within the ***./baseImage*** directory for more information.
- Make sure you also import the nginx-prometheus-exporter image from dockerhub. Else it will pull the from dockerhub everytime you deploy or when the pod restarts for whatever reason. 
    - More information can be found here: [nginx-prometheus-exporter:0.11](https://github.com/nginxinc/nginx-prometheus-exporter)
    - The Dockerhub Repository can be found here: [nginx-prometheus-exporter](https://hub.docker.com/r/nginx/nginx-prometheus-exporter/tags)
    - This reference case uses tag 0.11 found here: [nginx/nginx-prometheus-exporter:0.11](https://hub.docker.com/layers/nginx/nginx-prometheus-exporter/0.11/images/sha256-ebe927c25da25bbce77ff53c7ad559af534a06a904610e2886f3eeee7662198f)
- Make sure your namespace is allowed to get to your base Image and/or your nginx-prometheus-exporter image. Else, create a rolebinding, more information on how to do that within OpenShift [here](https://docs.openshift.com/container-platform/4.11/openshift_images/managing_images/using-image-pull-secrets.html).
- All templates contain default values, please make sure to review those values and change them accordingly before processing and/or applying the templates.
- All templates contain the default value for ${NAME} as 'ha-ping-pong', and ${ENVIRONMENT} as 'development'. This means that it will, by default, try to deploy any processed templatest o the namespace 'ha-ping-pong-development'. If you want to leave these to default, make sure to create a namespace with that name :smile:.


# The Base Image
This part describes the creation of the Base Image you can use to start building the application. Everything mentioned here will be located within the ***./baseImage*** directroy within this git repository.

Using the [/rhel8/nginx-120:latest](https://catalog.redhat.com/software/containers/search?q=rhel8%2Fnginx-120&p=1) from registry.redhat.io allows for a good starting point; a RHEL8 OS with OpenSource NGINX 1.2 preinstalled, including some [s2i](https://github.com/openshift/source-to-image) magic.

- First make sure you open the Dockerfile within the ***./baseImage*** directory. Check the "Certificates" section (line 31) and make sure you add your organization's endpoint ROOT-CA, DOMAIIN and any public key infrastrcture certificates. Doing this allows your container propperly to talk to any internally hosted applications via HTTPS. If you do not have this available to you, simply comment or remove this section (line 31-34).
- Now open the ***./baseImage/ha-nginx-pong-buildconfig.yaml*** and review the parameters (line 40+) at the bottom. You can either fill in your own correct value's here or pass variables along whilst processing this template.
- You can either process the template first, save the artifact and then apply it or simply pipe your processed template to the apply command as such:
```
oc process -f ./baseImage/ha-nginx-pong-buildconfig.yaml | oc apply -f -
```
- See the below example on how to pass parameters whilst piping the output to 'oc apply -f -' 
```
oc process -f ./baseImage/ha-nginx-pong-buildconfig.yaml -p NAME=IAmATeapot -p BRANCH=418 | oc apply -f -
```
- Once applied, OpenShift will contain the BuildConfig and ImageStream. Now you can start your build. Before starting the build, make sure to commit and push any changes you've made to your git repo as the buildconfig will use git to find the latest Dockerfile. You can either start a build from the OpenShift webinterface or as follows via the CLI:
```
oc start-build <buildconfig_name> -n namespace
```
```
#Example
oc start-build ha-ping-pong-base-image -n namespace
```
- Once this build is completed it will create the baseImage we need to be able to build and eventually deploy our application. It includes all the S2I magic within the baseImage/s2i directory. Click [here](https://github.com/openshift/source-to-image) for more information on s2i.

# Building and Deploying the NGINX and Prometheus Node Exporter application
This part describes the creation of the Base Image you can use to start building the application. Everything mentioned here will be located within the ***./openShift*** directroy within this git repository.

Let's use the baseImage we build to create the appliaction image that allows us to deploy this solution. Everything that you need can all be found within the ***./openshift*** directroy. The directory holds some static NGINX configuration files within the ***./openShift/conf*** directory. It also contains the the build and deploy templates within the ***./openShift/templates*** directory. 

***Note:*** _For this reference case you will not be required to adjust any of the static NGINX configuration files within the ***./openShift/conf*** directory, but i would strongly advise you to take a peek :smile:._

## The Build Template
- Open the build template  ***./openShift/templates/ha-nginx-pong-build-template.yaml***. Check out the parameters at the bottom. Keep the name variable consistent between all templates within this git repository for optimum compatability between the processed .yaml artifacts. Just as with the baseImage, you can either fill in your own correct values here (line 45+) or pass variables along whilst processing this template.
- You can either process the template first, save the artifact and then apply it or simply pipe your processed template to the apply command as such:
```
oc process -f ./openShift/templates/ha-nginx-pong-buildconfig.yaml | oc apply -f -
```
- Once applied, OpenShift will contain the BuildConfig and ImageStream. Now you can start your build. You can either start a build from the OpenShift webinterface or as follows via the CLI:
```
oc start-build <buildconfig_name> -n namespace
```
```
#Example
oc start-build ha-ping-pong -n namespace
```
- In this build we tell OpenShift to gather the NGINX configuration files within the ***./openShift/conf*** directory within your git repository. These will automatically be placed within your containerimage's ***/tmp/src/*** directory.

## The Deploy Template
- Open the build template ***./openShift/templates/ha-nginx-pong-deploy-template.yaml***. Check out the parameters at the bottom. Keep the name variable consistent between all templates within this git for optimum compatability between the artifacts. Just as with the baseImage, you can either fill in your own correct values here (line 174+) or pass variables along whilst processing this template.
- You can either process the template first, save the artifact and then apply it or simply pipe your processed template to the apply command as such:
```
oc process -f ./openShift/templates/ha-nginx-pong-buildconfig.yaml | oc apply -f -
```
- Once applied, OpenShift will contain the following objects: 
    - 1x HorizontalPodAutoscaler object
    - 2x Service object
    - 1x Route object
    - 1x DeploymentConfig object
    - 1x ServiceMonitor

- Below some more detailed information on these objects:
    - The [HorizontalPodAutoscaler](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/) takes case of all your scaling needs.
    - We create two [Service](https://kubernetes.io/docs/concepts/services-networking/service/) objects, one for NGINX traffic on port 8080 and one for all prometheus scrape traffic on port 9113.
    - A single [Route](https://docs.openshift.com/container-platform/4.11/networking/routes/route-configuration.html) object (OpenShift only) in order to expose our NGINX application to traffic coming from outside the openShift cluster. Adjust the ${ROUTE_HOST} variable to your needs, the default parameter defaults to: ha-ping-pong-development.yourcompany.local .
    - [DeploymentConfig](https://docs.openshift.com/container-platform/4.11/applications/deployments/what-deployments-are.html#deployments-and-deploymentconfigs_what-deployments-are) objects are OpenShift only, but OpenShift also allows for the Deployment object. Native Kubernetes supports Deployment objects only. In this DeploymentConfig we listed two "containers". One is the NGINX Application container and the other one is the Prometheus Node Exporter container. 
    - The remaining object is the [ServiceMonitor](https://docs.openshift.com/container-platform/4.11/rest_api/monitoring_apis/servicemonitor-monitoring-coreos-com-v1.html), this object tells Prometheus to scrape the /metrics target on port 9113

# Building and Deploying the Alert Template
This part describes the creation of the Base Image you can use to start building the application. Everything mentioned here will be located within the ***./alerts*** directory within this git repository.

The reason for seperating alerts from the rest is because (in my experience) you might want to build and deploy your alerts seperately as they might be subject to change more often than your application does. I usually use a seperate build/deploy pipeline for this and seperating it from the application directory makes that more clear for me. It also allows me to more easially trigger builds for the alerts .yaml file only. This way I prevent the complete solution from building, if the only thing I changed was the alerts .yaml file.

- Open the build template ***./alerts/ha-nginx-pong-prometheus-rule-alert-template.yaml***. Check out the parameters at the bottom. Keep the name variable consistent between all templates within this git for optimum compatability between the artifacts. Just as with the baseImage, you can either fill in your own correct value's here or pass variables along whilst processing this template.
- You can either process the template first, save the artifact and then apply it or simply pipe your processed template to the apply command as such:
```
oc process -f ./alerts/ha-nginx-pong-prometheus-rule-alert-template.yaml | oc apply -f -
```
- Once applied, OpenShift will contain the [PrometheusRule](https://docs.openshift.com/container-platform/4.11/monitoring/managing-alerts.html#creating-alerting-rules-for-user-defined-projects_managing-alerts) object. 

# Grafana Dashboard
This part describes the creation of the Base Image you can use to start building the application. Everything mentioned here will be located within the ***./grafana*** directory within this git repository.

It is possible to use grafana for displaying the metrics that you gather or the alerts that might fire. For that reason I have added a Grafana NGINX Prometheus Node Exporter example Dashboard to get you started. You can either use this with the Open Source Grafana operator or any other way you might deploy Grafana onto your cluster or for your DevOps team. See the Grafana JSON within the 'grafana' directory.

## Steps on how to import the Grafana Json
See the below steps in order to import your Grafana Dashboard:

1. Open Grafana and go to Dashboards
2. Klik on 'New'
3. Klik on 'Import'
<div align="center">
![import_grafana_1](/pictures/import_grafana_1.png "Import Grafana step 1, 2 and 3")
</div>

---

4. Copy & Paste the Grafana Json from the ***./grafana*** directory
5. Klik on 'Load'
<div align="center">
![import_grafana_2](/pictures/import_grafana_2.png "Import Grafana step 4 and 5")
</div>

---

6. Klik on 'Import'
<div align="center">
![import_grafana_3](/pictures/import_grafana_3.png "Import Grafana step 6")
</div>

---

## Example of the Grafana Json Dashboard
<div align="center">
![grafana_example_result](/pictures/grafana_example.png "grafana_example_result")
</div>
